<?php 
	$id_bio	= $_GET['id_bio'];

	require_once 'dbconnect.php';

	$query = "SELECT * FROM biodata WHERE id_bio=$id_bio";

	$stmt = mysqli_query($conn, $query);

	$result = array();

	$row = mysqli_fetch_array($stmt);
	array_push($result, array(
		"id_bio"	=> $row['id_bio'],
		"nama"		=> $row['nama'],
		"asal_skul"	=> $row['asal_skul'],
		"phone"		=> $row['phone'],
		"email"		=> $row['email'],
		"alamat"	=> $row['alamat']
		));

	echo json_encode(array('biodata'=>$result));

	mysqli_close($conn);
 ?>