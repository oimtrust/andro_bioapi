<?php 
	require_once 'dbconnect.php';

	$query = "SELECT * FROM biodata";

	$stmt = mysqli_query($conn, $query);

	$result = array();

	while ($row = mysqli_fetch_array($stmt)) {
		array_push($result,array(
			"id_bio"	=> $row['id_bio'],
			"nama"		=> $row['nama']
			));
	}

	echo json_encode(array('biodata'=>$result));
	mysqli_close($conn);
 ?>